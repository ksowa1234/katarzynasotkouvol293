﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallController : MonoBehaviour
{
    GameObject selected;
    Vector3 offset;

    void Update()
    {
        Vector3 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);

        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit))
                selected = hit.transform.gameObject;

           
        }

        if (selected)
        {
            selected.transform.position = mousePosition + offset;
        }

        if (Input.GetMouseButtonUp(0) && selected)
        {
            selected = null;
        }
    }
}
